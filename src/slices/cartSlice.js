import { createSlice, createAsyncThunk } from '@reduxjs/toolkit';
import { productsParser } from '../utilities';
import { API } from '../config';
const { DOMAIN, VERSION, OPTIONS: API_OPTIONS } = API;

const initialState = {
  items: [],
  recommended: [],
  error: false,
  loading: false,
  coupon: false,
  express_shipping: false,
  shipping_discount: false,
  free_shipping: false,
};

export const fetchItems = createAsyncThunk(
  'cart/fetchResultsStatus',
  async ({ query = '', options = {} }, { getState, rejectWithValue }) => {
    try {
      // const filters = getState().filters;
      const response = await fetch(`${DOMAIN}${VERSION}/pokemon/${query}`, {
        ...API_OPTIONS,
        ...options,
      });
      const results = await response.json();
      return productsParser(results.results);
    } catch (err) {
      return rejectWithValue(err.response.data);
    }
  }
);

const cartSlice = createSlice({
  name: 'cart',
  initialState: initialState,
  reducers: {
    modifyCart: (state, action) => {
      return { ...state, ...action.payload };
    },
    addItem: (state, action) => {
      state.items.push(action.payload);
    },
    removeItem: (state, action) => {
      state.items = state.items.filter(
        (items) => items.id != action.payload.id
      );
    },
  },
  extraReducers: {
    [fetchItems.fulfilled]: (state, action) => {
      state.loading = false;
      state.error = false;
      state.recommended = action.payload;
    },
    [fetchItems.pending]: (state, action) => {
      state.loading = true;
      state.error = false;
    },
    [fetchItems.rejected]: (state, action) => {
      state.loading = false;
      if (action.payload) {
        state.error = action.payload.errorMessage;
      } else {
        state.error = action.error.message;
      }
    },
  },
});

export default cartSlice.reducer;
// Actions
export const { modifyCart, addItem, removeItem } = cartSlice.actions;
