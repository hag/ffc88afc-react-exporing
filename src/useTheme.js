import { useState, createContext, useContext } from 'react';
import Brightness2Icon from '@material-ui/icons/Brightness2';
import Brightness7Icon from '@material-ui/icons/Brightness7';

const themes = {
  light: {
    foreground: '#000000',
    background: '#eeeeee',
  },
  dark: {
    foreground: '#ffffff',
    background: '#222222',
  },
};

export const ThemeContext = createContext();

export const ThemeProvider = ({ children }) => {
  const [theme, setTheme] = useState(themes.light);
  const [icon, setIcon] = useState(<Brightness2Icon />);

  const toggleTheme = () => {
    setTheme((theme) =>
      theme.background === themes.light.background ? themes.dark : themes.light
    );
    theme.background === themes.light.background
      ? setIcon(<Brightness7Icon />)
      : setIcon(<Brightness2Icon />);
  };

  return (
    <ThemeContext.Provider
      value={{ theme, toggleTheme, setTheme, themes, icon }}
    >
      {children}
    </ThemeContext.Provider>
  );
};

const useTheme = () => {
  const { theme, toggleTheme, setTheme, themes, icon } = useContext(
    ThemeContext
  );

  return { theme, toggleTheme, setTheme, themes, icon };
};

export default useTheme;
