import { useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import {
  IconButton,
  makeStyles,
  Badge,
  Popover,
  Grid,
  Checkbox,
  Box,
} from '@material-ui/core';
import { useHistory } from 'react-router-dom';
import ShoppingCartIcon from '@material-ui/icons/ShoppingCart';
import DeleteIcon from '@material-ui/icons/Delete';
import LocalShippingIcon from '@material-ui/icons/LocalShipping';
import { modifyCart, removeItem } from '../../slices/cartSlice';
import ProductViewsWithPrice from '../ProductViews';

const useStyles = makeStyles((theme) => ({
  cartPop: {
    padding: 10,
    minWidth: 400,
  },
  menuButton: {
    color: 'white',
    marginRight: theme.spacing(2),
  },
  link: {
    textDecoration: 'none',
  },
}));

const CartAppBar = () => {
  const [anchorEl, setAnchorEl] = useState(null);
  const history = useHistory();
  const classes = useStyles();
  const dispatch = useDispatch();

  const cart = useSelector((state) => state.cart);
  const recommended = cart.recommended;
  const cartItems = cart.items ?? [];
  const expressShipping = cart.express_shipping ?? false;

  const handleFastShippingClick = (e) => {
    dispatch(modifyCart({ express_shipping: e.target.checked }));
  };

  const handleCartDelete = (id) => {
    dispatch(removeItem({ id }));
  };

  const handleClick = (event) => {
    setAnchorEl(event.currentTarget);
  };

  const handleClose = () => {
    setAnchorEl(null);
  };

  const open = Boolean(anchorEl);
  const id = open ? 'simple-popover' : undefined;

  let totalPrice = cartItems.reduce((acc, item) => acc + item.price, 0);
  totalPrice += expressShipping ? 1000 : 0;

  return (
    <div>
      <IconButton
        edge='start'
        className={classes.menuButton}
        aria-label='menu'
        onClick={handleClick}
      >
        <Badge badgeContent={cartItems.length} color='primary'>
          <ShoppingCartIcon />
        </Badge>
      </IconButton>
      <Popover
        id={id}
        disablePortal={false}
        open={open}
        anchorEl={anchorEl}
        onClose={handleClose}
        anchorOrigin={{
          vertical: 'bottom',
          horizontal: 'center',
        }}
        transformOrigin={{
          vertical: 'top',
          horizontal: 'center',
        }}
      >
        <Grid container direction='column' className={classes.cartPop}>
          {cartItems.length ? (
            cartItems.map((item, index) => (
              <Grid item key={index}>
                <Grid container>
                  <Grid item xs={1}>
                    {index + 1}:
                  </Grid>
                  <Grid item xs={8}>
                    {item.title}
                  </Grid>
                  <Grid item xs={2}>
                    <ProductViewsWithPrice price={item.price} />
                  </Grid>
                  <Grid item xs={1}>
                    <IconButton
                      aria-label='delete'
                      size='small'
                      onClick={() => handleCartDelete(item.id)}
                    >
                      <DeleteIcon fontSize='small' />
                    </IconButton>
                  </Grid>
                </Grid>
              </Grid>
            ))
          ) : (
            <Grid item>
              <Box display='flex' justifyContent='center' alignItems='center'>
                Cart is empty
              </Box>
            </Grid>
          )}
          {cartItems.length > 0 ? (
            <Grid container>
              <Grid item xs={8}>
                <Box display='flex' gridGap={3} alignItems='center'>
                  <Checkbox
                    checked={expressShipping}
                    onChange={handleFastShippingClick}
                    inputProps={{ 'aria-label': 'primary checkbox' }}
                  />
                  <LocalShippingIcon fontSize='small' />
                  Express Shipping
                </Box>
              </Grid>
              <Grid item xs={4}>
                <Box p={2} borderTop={1}>
                  Total:
                  <strong>
                    <em>
                      <ProductViewsWithPrice price={totalPrice} />
                    </em>
                  </strong>
                </Box>
              </Grid>
            </Grid>
          ) : null}
        </Grid>
      </Popover>
    </div>
  );
};

export default CartAppBar;
