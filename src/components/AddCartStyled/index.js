import React from 'react';
import styled from 'styled-components';

const Button = styled.button`
  font-size: 1.5em;
  text-align: center;
  color: #2ba6df;
  border: 1px solid #2aa1d8;
`;

const EmojiButton = styled(Button)`
  transition: width 1s, height 4s;
  width: 300px;

  &:hover {
    color: #ff3737;
    border: 1px solid #e73131;
    width: 400px;
    &::after {
      opacity: 1;
    }
  }

  &::after {
    content: '${({ cta }) => cta || ''}';
    opacity: 0;
    transition: opacity 0.5s;
  }
`;

const AddCartStyled = ({ text, emoji, ...props }) => {
  return (
    <EmojiButton cta={emoji} {...props}>
      {text}
    </EmojiButton>
  );
};

export default AddCartStyled;
