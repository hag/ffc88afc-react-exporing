import React from 'react';
import {
  Toolbar,
  IconButton,
  Typography,
  Menu,
  MenuItem,
  AppBar,
  makeStyles,
  Button,
} from '@material-ui/core';
import { Link } from 'react-router-dom';

import MenuIcon from '@material-ui/icons/Menu';
import AccountCircle from '@material-ui/icons/AccountCircle';
import TagFacesIcon from '@material-ui/icons/TagFaces';
import Brightness2Icon from '@material-ui/icons/Brightness2';
import useTheme from '../../useTheme';
import { CartAppBar } from '../Cart';
const useStyles = makeStyles((theme) => ({
  root: {
    flexGrow: 1,
  },
  header: {
    color: 'white',
    backgroundColor: '#2ba6df',
    paddingTop: '1rem',
  },
  menuButton: {
    color: 'white',
    marginRight: theme.spacing(2),
  },
  title: {
    textAlign: 'initial',
    flexGrow: 1,
  },
  login: {
    color: 'white',
    textDecoration: 'none',
    textTransform: 'capitalize',
  },
  link: {
    color: 'inherit',
    textDecoration: 'inherit',
  },
}));

const AppBarComponent = () => {
  const { toggleTheme, icon: Icon } = useTheme();
  const classes = useStyles();
  return (
    <div className={classes.root}>
      <AppBar position='static' className={classes.header} elevation={0}>
        <Toolbar>
          <Link to='/'>
            <IconButton
              edge='start'
              className={classes.menuButton}
              aria-label='menu'
            >
              <TagFacesIcon />
            </IconButton>
          </Link>
          <Typography variant='h6' className={classes.title}>
            RMarket
          </Typography>
          <CartAppBar />
          <IconButton
            edge='start'
            className={classes.menuButton}
            aria-label='menu'
            onClick={() => toggleTheme()}
          >
            {Icon}
          </IconButton>
          <Link to='/register' className={classes.link}>
            <Button className={classes.login} color='inherit'>
              Εγγραφή
            </Button>
          </Link>
          <Button className={classes.login} color='inherit'>
            Σύνδεση
          </Button>
        </Toolbar>
      </AppBar>
    </div>
  );
};

export default AppBarComponent;
