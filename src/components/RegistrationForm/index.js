import { useForm, Controller } from 'react-hook-form';
import Avatar from '@material-ui/core/Avatar';
import Button from '@material-ui/core/Button';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import Checkbox from '@material-ui/core/Checkbox';
import Link from '@material-ui/core/Link';
import Grid from '@material-ui/core/Grid';
import Box from '@material-ui/core/Box';
import LockOutlinedIcon from '@material-ui/icons/LockOutlined';
import Typography from '@material-ui/core/Typography';
import { makeStyles } from '@material-ui/core/styles';
import Container from '@material-ui/core/Container';
import TextField from '@material-ui/core/TextField';

import useTheme from '../../useTheme';

const emailPattern = /^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,}$/i;
const defaultValues = {
  firstName: '',
  lastName: '',
  userName: '',
  email: '',
  birthDay: '2000-01-01',
  password: '',
  retypePassword: '',
  marketing: false,
};
const useStyles = makeStyles((theme) => ({
  paper: {
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
    minHeight: '100vh',
    backgroundColor: (props) => props.background,
    color: (props) => props.foreground,
  },
  avatar: {
    margin: theme.spacing(1),
    backgroundColor: theme.palette.secondary.main,
  },
  form: {
    width: '100%', // Fix IE 11 issue.
    marginTop: theme.spacing(3),
    '& label': {
      display: 'block',
      fontStyle: 'italic',
      marginLeft: 'inherit',
    },
    '& em': {
      display: 'block',
      fontStyle: 'italic',
      color: 'red',
    },
    '& >div': {
      padding: theme.spacing(2),
    },
  },
  submit: {
    margin: theme.spacing(3, 0, 2),
  },
}));

const RegistrationForm = () => {
  const { theme } = useTheme();
  const classes = useStyles(theme);

  const {
    handleSubmit,
    register,
    reset,
    watch,
    errors,
    control,
    getValues,
  } = useForm({
    defaultValues,
  });

  const watchMarketing = watch('marketing', false);

  const onSubmit = (data) => {
    console.log(data);
    reset();
  };

  const passwordValidation = (value) => {
    if (value === getValues()['password']) {
      return true;
    } else {
      return false;
    }
  };

  return (
    <Container component='main' maxWidth='xs'>
      <div className={classes.paper}>
        <Avatar className={classes.avatar}>
          <LockOutlinedIcon />
        </Avatar>
        <Typography component='h1' variant='h5'>
          Join us today!
        </Typography>
        <form
          className={classes.form}
          onSubmit={handleSubmit(onSubmit)}
          noValidate
        >
          <Grid container spacing={2}>
            <Grid item xs={12} sm={6}>
              <Controller
                as={TextField}
                variant='outlined'
                name='firstName'
                label='First Name'
                rules={{ required: true, minLength: 5 }}
                error={!!errors.firstName}
                control={control}
                helperText={errors.firstName && <em>First Name is required</em>}
              />
            </Grid>
            <Grid item xs={12} sm={6}>
              <label htmlFor='lastName'>Last Name</label>
              <input
                name='lastName'
                ref={register({ required: true, minLength: 2 })}
              />
              {errors.lastName && <em>Last Name is required</em>}
            </Grid>
            <Grid item xs={12} sm={6}>
              <label htmlFor='userName'>Username</label>
              <input
                name='userName'
                ref={register({ required: true, minLength: 2 })}
              />
              {errors.userName && <em>Username is required</em>}
            </Grid>
            <Grid item xs={12} sm={6}>
              <label htmlFor='birthDay'>Birthday</label>

              <input
                name='birthDay'
                type='date'
                ref={register({ required: true })}
              />
              {errors.birthDay && <em>Birthday is required</em>}
            </Grid>
            <Grid item xs={12}>
              <label htmlFor='email'>Email Address</label>

              <input
                name='email'
                ref={register({ required: true, pattern: emailPattern })}
              />
              {errors.email && <em>Please add a valid email</em>}
            </Grid>
            <Grid item xs={12}>
              <label htmlFor='password'>Password</label>
              <input
                type='password'
                name='password'
                ref={register({ required: true, minLength: 2 })}
              />
              {errors.password && <em>Password is required</em>}
            </Grid>
            <Grid item xs={12}>
              <label htmlFor='retypePassword'>Retype Password</label>
              <input
                type='password'
                name='retypePassword'
                ref={register({
                  validate: passwordValidation,
                  required: true,
                })}
              />
              {errors?.retypePassword ? (
                errors.retypePassword.type === 'validate' ? (
                  <em>Passwords must match</em>
                ) : (
                  <em>Retype Password is required</em>
                )
              ) : (
                ''
              )}
            </Grid>
            <Grid item xs={12}>
              <Controller
                name='marketing'
                errors={errors}
                control={control}
                render={(props) => (
                  <FormControlLabel
                    value='marketing'
                    control={
                      <Checkbox
                        onChange={(e) => props.onChange(e.target.checked)}
                        checked={props.value}
                      />
                    }
                    label='I want to receive inspiration, marketing promotions and updates via email.'
                  />
                )}
              />
            </Grid>
          </Grid>
          <Button
            type='submit'
            fullWidth
            variant='contained'
            color='primary'
            className={classes.submit}
            disabled={!watchMarketing}
          >
            Sign Up
          </Button>
          <Grid container justify='flex-end'>
            <Grid item>
              <Link href='#' variant='body2'>
                Already have an account? Sign in
              </Link>
            </Grid>
          </Grid>
        </form>
      </div>
    </Container>
  );
};

export default RegistrationForm;
