import { useState, useEffect } from 'react';
import ProductViewsWithPrice from '../ProductViews';
import useProductCount from '../../useProductCount';

const initialDiscounts = [
  { count: 4, discount: 10 },
  { count: 5, discount: 20 },
];

const AddProductCount = ({ price }) => {
  const [productPrice, productCount, setProductCount] = useProductCount(
    { price },
    { discounts: initialDiscounts }
  );

  return (
    <div>
      <button
        onClick={() => setProductCount((count) => count - 1)}
        disabled={!(productCount - 1)}
      >
        -
      </button>
      <input value={productCount} readOnly />
      <button
        onClick={() => setProductCount((count) => count + 1)}
        disabled={productCount >= 5 ? true : false}
      >
        +
      </button>
      <div>
        Price: <ProductViewsWithPrice price={productPrice} />
      </div>
      {productCount > 4 ? <div>You cannot order more than 5 items</div> : null}
      {initialDiscounts &&
        initialDiscounts.map((discount) => (
          <div key={discount.count}>
            <em>
              {discount.count} and up: you have {discount.discount}% off
            </em>
          </div>
        ))}
    </div>
  );
};

export default AddProductCount;
