import { useState, useEffect } from 'react';
import useTheme from '../../useTheme';

const ToggleTheme = () => {
  const { theme, themes, setTheme } = useTheme();

  useEffect(() => {
    console.log('always on');
  });

  useEffect(() => {
    console.log('only once');
    return () => console.log('close connection cleanup');
  }, []);

  useEffect(() => {
    console.log('every time theme change');
  }, [theme]);

  const toggleTheme = () => {
    setTheme((theme) =>
      theme.background === themes.light.background ? themes.dark : themes.light
    );
  };

  return (
    <div
      style={{
        backgroundColor: theme.background,
        minHeight: 10,
        minwidth: 10,
      }}
    >
      <button onClick={toggleTheme}>Toggle theme</button>
    </div>
  );
};

export default ToggleTheme;
