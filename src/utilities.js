export const productParser = ({ id, name: title, sprites }) => {
  const image = sprites.other['official-artwork'].front_default;
  const price = Math.floor(Math.random() * 10000) + 1;

  return { id, title, image, price };
};

export const productsParser = (results) => {
  return results.map(({ name: title, url }) => {
    let id = url.match(/pokemon\/([0-9]{1,4})/i)[1];
    const price = Math.floor(Math.random() * 10000) + 1;

    return { id, title, price };
  });
};
