import { useEffect, useContext } from 'react';
import { Paper, Grid, Box, makeStyles, Chip } from '@material-ui/core';
import { useParams, useHistory, Link } from 'react-router-dom';
import AddProductCount from '../../components/AddProductCount';
import ProductViewsWithPrice from '../../components/ProductViews';
import useTheme from '../../useTheme';
import AddCartStyled from '../../components/AddCartStyled';
import useFetch from '../../useFetch';

const useStyles = makeStyles({
  paper: (props) => ({
    padding: 10,
    minHeight: '100vh',
    backgroundColor: props.background,
    color: props.foreground,
  }),
  chip: (props) => ({
    backgroundColor: props.background,
    color: props.foreground,
  }),
  link: {
    textDecoration: 'none',
  },
});

const HomePage = () => {
  const { theme } = useTheme();
  const classes = useStyles(theme);
  const { product: products, loading } = useFetch('');

  return (
    <>
      <Paper className={classes.paper}>
        <Grid container spacing={3}>
          {products &&
            products.length > 0 &&
            products.map(({ id, title }) => (
              <Grid key={id} item xs={3}>
                <Link to={`/product/${title}`} className={classes.link}>
                  <Chip
                    className={classes.chip}
                    variant='outlined'
                    label={title}
                  />
                </Link>
              </Grid>
            ))}
        </Grid>
      </Paper>
    </>
  );
};

export default HomePage;
