import React from 'react';
import ProductViewsWithPrice from '../../components/ProductViews';
import { Grid, Box, Button } from '@material-ui/core';
import { Link as RouterLink } from 'react-router-dom';

const Recommended = ({ recommended }) => {
  return (
    recommended.length > 0 &&
    recommended
      .slice(0, recommended.length > 5 ? 5 : recommended.length)
      .map((item, index) => (
        <Grid item key={index}>
          <Button
            color='primary'
            component={RouterLink}
            to={`/product/${item.title}`}
          >
            <Box>
              <Grid item xs={8}>
                {item.title}
              </Grid>
              <Grid item xs={4}>
                <ProductViewsWithPrice price={item.price} />
              </Grid>
            </Box>
          </Button>
        </Grid>
      ))
  );
};
export default Recommended;
