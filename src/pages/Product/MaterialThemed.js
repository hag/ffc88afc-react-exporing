import { useEffect } from 'react';
import {
  Paper,
  Grid,
  Box,
  makeStyles,
  CircularProgress,
} from '@material-ui/core';
import { useParams, useHistory, useLocation, Link } from 'react-router-dom';
import { useDispatch, useSelector } from 'react-redux';
import {
  modifyCart,
  addItem,
  removeItem,
  fetchItems,
} from '../../slices/cartSlice';
import AddProductCount from '../../components/AddProductCount';
import ProductViewsWithPrice from '../../components/ProductViews';
import useTheme from '../../useTheme';
import AddCartStyled from '../../components/AddCartStyled';
import useFetch from '../../useFetch';
import Recommended from './Recommended';

const useStyles = makeStyles((theme) => ({
  paper: (props) => ({
    minHeight: '100vh',
    backgroundColor: props.background,
    color: props.foreground,
  }),
  image: {
    maxHeight: 300,
    borderRadius: '1.75rem',
    padding: '.35rem',
    '&:hover': {
      // backgroundColor: ({cti}) => cti,
      backgroundColor: '#2aa2da2b',
    },
  },
}));

const MaterialThemed = () => {
  let { id } = useParams();
  let history = useHistory();
  const { theme } = useTheme();
  const dispatch = useDispatch();
  const { product, loading, setQuery } = useFetch(id, { method: 'GET' });
  const classes = useStyles(theme);

  const cart = useSelector((state) => state.cart);
  const recommended = cart.recommended ?? [];

  useEffect(() => {
    if (!loading && !product) {
      history.push('/');
    }
  }, [loading, product]);

  useEffect(() => {
    setQuery(id);
  }, [id]);

  useEffect(() => {
    dispatch(fetchItems({}));
  }, []);

  const addToCart = () => {
    dispatch(addItem({ id, title: product?.title, price: product?.price }));
  };

  return (
    <>
      <Paper className={classes.paper}>
        <Grid container>
          <Grid item xs={12} md={4}>
            {!loading && product?.image ? (
              <img className={classes.image} src={product?.image} />
            ) : (
              <CircularProgress />
            )}
          </Grid>
          <Grid item xs={12} md={8}>
            {loading ? (
              <CircularProgress />
            ) : (
              <>
                <Box m={1}>
                  ID:{product?.id} - {product?.title}
                </Box>
                <Box m={1}>
                  <ProductViewsWithPrice price={product?.price} />
                </Box>
                <Box m={1}>
                  Pellentesque habitant morbi tristique senectus et netus et
                  malesuada fames ac turpis egestas. Vestibulum tortor quam,
                  feugiat vitae, ultricies eget, tempor sit amet, ante. Donec eu
                  libero sit amet quam egestas semper. Aenean ultricies mi vitae
                  est. Mauris placerat eleifend leo.
                </Box>
                <Box m={1}>
                  <AddProductCount price={product?.price} />
                </Box>
                <Box m={1}>
                  <AddCartStyled
                    onClick={addToCart}
                    text='Buy now'
                    emoji='⚡'
                  />
                </Box>
              </>
            )}
          </Grid>
          <Grid item xs={12}>
            <Box p={2} borderTop={1}>
              Recommended
            </Box>
            <Grid container direction='row' alignItems='center' spacing={4}>
              <Recommended recommended={recommended} />
            </Grid>
          </Grid>
        </Grid>
      </Paper>
    </>
  );
};

export default MaterialThemed;
