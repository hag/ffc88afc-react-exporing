import { useContext } from 'react';
import { useParams } from 'react-router-dom';
import AddProductCount from '../../components/AddProductCount';
import ProductViewsWithPrice from '../../components/ProductViews';
import { ThemeContext } from '../../useTheme';
import AddCartStyled from '../../components/AddCartStyled';

import useTheme from '../../useTheme';

const ProductPage = () => {
  // const { theme } = useTheme();
  const { theme } = useContext(ThemeContext);
  let { id } = useParams();
  return (
    <>
      <div
        style={{
          backgroundColor: theme.background,
          color: theme.foreground,
          minHeight: 10,
          minWidth: 10,
        }}
        className='product'
      >
        <div className='product__image'>
          <img src='https://picsum.photos/300/300' />
        </div>
        <div className='product-details'>
          <div className='product-details__title'>Ship Your Idea id:{id}</div>
          <div className='product-details__price'>
            <ProductViewsWithPrice price={930} />
          </div>
          <div className='product-details__description'>
            Pellentesque habitant morbi tristique senectus et netus et malesuada
            fames ac turpis egestas. Vestibulum tortor quam, feugiat vitae,
            ultricies eget, tempor sit amet, ante. Donec eu libero sit amet quam
            egestas semper. Aenean ultricies mi vitae est. Mauris placerat
            eleifend leo.
          </div>
          <div className='product-details__count'>
            <AddProductCount price={930} />
          </div>
          <div className='product-details__review'></div>
          <div className='product-details__add-cart'>
            <AddCartStyled text='Buy now' emoji='⚡' />
          </div>
        </div>
      </div>
      <div className='related-products'></div>
    </>
  );
};

export default ProductPage;
