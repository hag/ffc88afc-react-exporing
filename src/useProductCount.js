import { useState, useEffect } from 'react';

const useProductCount = ({ price }, { discounts } = {}) => {
  const [productCount, setProductCount] = useState(1);
  const [productPrice, setProductPrice] = useState(price);

  useEffect(() => {
    console.log('%c[Update] useEffect 🔁', 'color: aqua');

    let calculatedPrice = price;
    if (discounts) {
      discounts.forEach((discount) => {
        if (productCount >= discount.count) {
          calculatedPrice = price - price * (discount.discount / 100);
        }
      });
    }
    setProductPrice(() => productCount * calculatedPrice);

    return () => console.log('%c[Cleanup] the useEffect', 'color: tomato');
  }, [productCount]);

  return [productPrice, productCount, setProductCount];
};

export default useProductCount;
