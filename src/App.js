import { BrowserRouter as Router, Link, Route, Switch } from 'react-router-dom';
import { Box, Container } from '@material-ui/core';
import LikeBox from './components/LikeBox';
import AddProductCount from './components/AddProductCount';
import ProductViewsWithPrice from './components/ProductViews';
import ToggleTheme from './components/ToggleTheme';
import AppBar from './components/AppBar';
import ProductPage from './pages/Product';
import HomePage from './pages/Home';
import RegistrationPage from './pages/Register';
import logo from './logo.svg';
import './App.css';

function App() {
  // return <LikeBox views={100} />;
  return (
    <div className='App'>
      <Router>
        <Container elevation={0}>
          <AppBar />
        </Container>
        <Container fixed>
          <Switch>
            <Route exact path='/'>
              <HomePage />
            </Route>
            <Route path='/register'>
              <RegistrationPage />
            </Route>
            {/* <Route path='/product/:id' render={() => <ProductPage />} /> */}
            <Route path='/product/:id'>
              <ProductPage />
            </Route>
          </Switch>
          <Box p={1} align='center'>
            Made with ❤️
          </Box>
        </Container>
      </Router>
    </div>
  );
}

export default App;
