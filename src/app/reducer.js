import { combineReducers } from '@reduxjs/toolkit';
import cart from '../slices/cartSlice';

const rootReducer = combineReducers({ cart });
export default rootReducer;
