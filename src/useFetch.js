import { useState, useEffect } from 'react';
import { productParser, productsParser } from './utilities';
import { API } from './config';

function useFetch(url, options = {}) {
  const [loading, setLoading] = useState(false);
  const [product, setProduct] = useState({});
  const [query, setQuery] = useState(url);

  const { DOMAIN, VERSION, OPTIONS: API_OPTIONS } = API;
  useEffect(() => {
    const getProduct = async () => {
      try {
        setLoading(true);
        const response = await fetch(`${DOMAIN}${VERSION}/pokemon/${query}`, {
          ...API_OPTIONS,
          ...options,
        });
        const results = await response.json();
        setProduct(() =>
          results.results
            ? productsParser(results.results)
            : productParser(results)
        );
        setLoading(false);
      } catch (error) {
        setLoading(false);
        setProduct(null);
      }
    };

    getProduct();
  }, [query]);

  return { loading, product, query, setQuery };
}

export default useFetch;
