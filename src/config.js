export const API = {
  DOMAIN: 'https://pokeapi.co',
  VERSION: '/api/v2',
  OPTIONS: {
    method: 'GET', // *GET, POST, PUT, DELETE, etc.
    headers: {
      'Content-Type': 'application/json',
      // 'Content-Type': 'application/x-www-form-urlencoded',
    },
  },
};
